#include <stdio.h>
#include <stdlib.h>

int nextNumber(char **str);
int nextHigh(char **str, int nr);
int nextLow(char **str, int nr);

int main(int argc, char **argv) {
    char *str = argv[1];
    int nr = nextNumber(&str);
    printf("%d", nextLow(&str, nr));
}

int nextNumber(char **str) {
    if(**str == '-') {
        *str += 1;
        return -nextNumber(str);
    } else {
        int nr = atoi(*str);
        while((*++*str >= '0' && **str <= '9') || **str == ' ');
        return nr;
    }
}

int nextHigh(char **str, int nr) {
    while(1) {
        if(**str == '*') {
            *str += 1;
            nr *= nextNumber(str);
        } else if(**str == '/') {
            *str += 1;
            nr /= nextNumber(str);
        } else {
            break;
        }
    }
    return nr;
}

int nextLow(char **str, int nr) {
    nr = nextHigh(str, nr);
    while(1) {
        if(**str == '+') {
            *str += 1;
            int temp_nr = nextNumber(str);
            nr += nextHigh(str, temp_nr);
        } else if(**str == '-') {
            *str += 1;
            int temp_nr = nextNumber(str);
            nr -= nextHigh(str, temp_nr);
        } else {
            break;
        }
    }
    return nr;
}
